import resolve from "@rollup/plugin-node-resolve"
import { terser } from "rollup-plugin-terser"

const terserOptions = {
  mangle: false,
  compress: false,
  format: {
    beautify: true,
    indent_level: 2
  }
}

export default [
  {
    input: "src/index.js",
    output: [
      {
        file: "dist/stimulus-reveal.js",
        format: "umd",
        name: "RevealController"
      },

      {
        file: "dist/stimulus-reveal.esm.js",
        format: "es"
      }
    ],
    plugins: [
      resolve(),
      terser(terserOptions)
    ]
  }
];
