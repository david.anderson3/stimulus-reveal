(function(global, factory) {
  typeof exports === "object" && typeof module !== "undefined" ? module.exports = factory() : typeof define === "function" && define.amd ? define(factory) : (global = typeof globalThis !== "undefined" ? globalThis : global || self, 
  global.RevealController = factory());
})(this, (function() {
  "use strict";
  function camelize(value) {
    return value.replace(/(?:[_-])([a-z0-9])/g, ((_, char) => char.toUpperCase()));
  }
  function capitalize(value) {
    return value.charAt(0).toUpperCase() + value.slice(1);
  }
  function dasherize(value) {
    return value.replace(/([A-Z])/g, ((_, char) => `-${char.toLowerCase()}`));
  }
  function readInheritableStaticArrayValues(constructor, propertyName) {
    const ancestors = getAncestorsForConstructor(constructor);
    return Array.from(ancestors.reduce(((values, constructor) => {
      getOwnStaticArrayValues(constructor, propertyName).forEach((name => values.add(name)));
      return values;
    }), new Set));
  }
  function readInheritableStaticObjectPairs(constructor, propertyName) {
    const ancestors = getAncestorsForConstructor(constructor);
    return ancestors.reduce(((pairs, constructor) => {
      pairs.push(...getOwnStaticObjectPairs(constructor, propertyName));
      return pairs;
    }), []);
  }
  function getAncestorsForConstructor(constructor) {
    const ancestors = [];
    while (constructor) {
      ancestors.push(constructor);
      constructor = Object.getPrototypeOf(constructor);
    }
    return ancestors.reverse();
  }
  function getOwnStaticArrayValues(constructor, propertyName) {
    const definition = constructor[propertyName];
    return Array.isArray(definition) ? definition : [];
  }
  function getOwnStaticObjectPairs(constructor, propertyName) {
    const definition = constructor[propertyName];
    return definition ? Object.keys(definition).map((key => [ key, definition[key] ])) : [];
  }
  (() => {
    function extendWithReflect(constructor) {
      function extended() {
        return Reflect.construct(constructor, arguments, new.target);
      }
      extended.prototype = Object.create(constructor.prototype, {
        constructor: {
          value: extended
        }
      });
      Reflect.setPrototypeOf(extended, constructor);
      return extended;
    }
    function testReflectExtension() {
      const a = function() {
        this.a.call(this);
      };
      const b = extendWithReflect(a);
      b.prototype.a = function() {};
      return new b;
    }
    try {
      testReflectExtension();
      return extendWithReflect;
    } catch (error) {
      return constructor => class extended extends constructor {};
    }
  })();
  function ClassPropertiesBlessing(constructor) {
    const classes = readInheritableStaticArrayValues(constructor, "classes");
    return classes.reduce(((properties, classDefinition) => Object.assign(properties, propertiesForClassDefinition(classDefinition))), {});
  }
  function propertiesForClassDefinition(key) {
    return {
      [`${key}Class`]: {
        get() {
          const {classes: classes} = this;
          if (classes.has(key)) {
            return classes.get(key);
          } else {
            const attribute = classes.getAttributeName(key);
            throw new Error(`Missing attribute "${attribute}"`);
          }
        }
      },
      [`${key}Classes`]: {
        get() {
          return this.classes.getAll(key);
        }
      },
      [`has${capitalize(key)}Class`]: {
        get() {
          return this.classes.has(key);
        }
      }
    };
  }
  function TargetPropertiesBlessing(constructor) {
    const targets = readInheritableStaticArrayValues(constructor, "targets");
    return targets.reduce(((properties, targetDefinition) => Object.assign(properties, propertiesForTargetDefinition(targetDefinition))), {});
  }
  function propertiesForTargetDefinition(name) {
    return {
      [`${name}Target`]: {
        get() {
          const target = this.targets.find(name);
          if (target) {
            return target;
          } else {
            throw new Error(`Missing target element "${name}" for "${this.identifier}" controller`);
          }
        }
      },
      [`${name}Targets`]: {
        get() {
          return this.targets.findAll(name);
        }
      },
      [`has${capitalize(name)}Target`]: {
        get() {
          return this.targets.has(name);
        }
      }
    };
  }
  function ValuePropertiesBlessing(constructor) {
    const valueDefinitionPairs = readInheritableStaticObjectPairs(constructor, "values");
    const propertyDescriptorMap = {
      valueDescriptorMap: {
        get() {
          return valueDefinitionPairs.reduce(((result, valueDefinitionPair) => {
            const valueDescriptor = parseValueDefinitionPair(valueDefinitionPair);
            const attributeName = this.data.getAttributeNameForKey(valueDescriptor.key);
            return Object.assign(result, {
              [attributeName]: valueDescriptor
            });
          }), {});
        }
      }
    };
    return valueDefinitionPairs.reduce(((properties, valueDefinitionPair) => Object.assign(properties, propertiesForValueDefinitionPair(valueDefinitionPair))), propertyDescriptorMap);
  }
  function propertiesForValueDefinitionPair(valueDefinitionPair) {
    const definition = parseValueDefinitionPair(valueDefinitionPair);
    const {key: key, name: name, reader: read, writer: write} = definition;
    return {
      [name]: {
        get() {
          const value = this.data.get(key);
          if (value !== null) {
            return read(value);
          } else {
            return definition.defaultValue;
          }
        },
        set(value) {
          if (value === undefined) {
            this.data.delete(key);
          } else {
            this.data.set(key, write(value));
          }
        }
      },
      [`has${capitalize(name)}`]: {
        get() {
          return this.data.has(key) || definition.hasCustomDefaultValue;
        }
      }
    };
  }
  function parseValueDefinitionPair([token, typeDefinition]) {
    return valueDescriptorForTokenAndTypeDefinition(token, typeDefinition);
  }
  function parseValueTypeConstant(constant) {
    switch (constant) {
     case Array:
      return "array";

     case Boolean:
      return "boolean";

     case Number:
      return "number";

     case Object:
      return "object";

     case String:
      return "string";
    }
  }
  function parseValueTypeDefault(defaultValue) {
    switch (typeof defaultValue) {
     case "boolean":
      return "boolean";

     case "number":
      return "number";

     case "string":
      return "string";
    }
    if (Array.isArray(defaultValue)) return "array";
    if (Object.prototype.toString.call(defaultValue) === "[object Object]") return "object";
  }
  function parseValueTypeObject(typeObject) {
    const typeFromObject = parseValueTypeConstant(typeObject.type);
    if (typeFromObject) {
      const defaultValueType = parseValueTypeDefault(typeObject.default);
      if (typeFromObject !== defaultValueType) {
        throw new Error(`Type "${typeFromObject}" must match the type of the default value. Given default value: "${typeObject.default}" as "${defaultValueType}"`);
      }
      return typeFromObject;
    }
  }
  function parseValueTypeDefinition(typeDefinition) {
    const typeFromObject = parseValueTypeObject(typeDefinition);
    const typeFromDefaultValue = parseValueTypeDefault(typeDefinition);
    const typeFromConstant = parseValueTypeConstant(typeDefinition);
    const type = typeFromObject || typeFromDefaultValue || typeFromConstant;
    if (type) return type;
    throw new Error(`Unknown value type "${typeDefinition}"`);
  }
  function defaultValueForDefinition(typeDefinition) {
    const constant = parseValueTypeConstant(typeDefinition);
    if (constant) return defaultValuesByType[constant];
    const defaultValue = typeDefinition.default;
    if (defaultValue !== undefined) return defaultValue;
    return typeDefinition;
  }
  function valueDescriptorForTokenAndTypeDefinition(token, typeDefinition) {
    const key = `${dasherize(token)}-value`;
    const type = parseValueTypeDefinition(typeDefinition);
    return {
      type: type,
      key: key,
      name: camelize(key),
      get defaultValue() {
        return defaultValueForDefinition(typeDefinition);
      },
      get hasCustomDefaultValue() {
        return parseValueTypeDefault(typeDefinition) !== undefined;
      },
      reader: readers[type],
      writer: writers[type] || writers.default
    };
  }
  const defaultValuesByType = {
    get array() {
      return [];
    },
    boolean: false,
    number: 0,
    get object() {
      return {};
    },
    string: ""
  };
  const readers = {
    array(value) {
      const array = JSON.parse(value);
      if (!Array.isArray(array)) {
        throw new TypeError("Expected array");
      }
      return array;
    },
    boolean(value) {
      return !(value == "0" || value == "false");
    },
    number(value) {
      return Number(value);
    },
    object(value) {
      const object = JSON.parse(value);
      if (object === null || typeof object != "object" || Array.isArray(object)) {
        throw new TypeError("Expected object");
      }
      return object;
    },
    string(value) {
      return value;
    }
  };
  const writers = {
    default: writeString,
    array: writeJSON,
    object: writeJSON
  };
  function writeJSON(value) {
    return JSON.stringify(value);
  }
  function writeString(value) {
    return `${value}`;
  }
  class Controller {
    constructor(context) {
      this.context = context;
    }
    static get shouldLoad() {
      return true;
    }
    get application() {
      return this.context.application;
    }
    get scope() {
      return this.context.scope;
    }
    get element() {
      return this.scope.element;
    }
    get identifier() {
      return this.scope.identifier;
    }
    get targets() {
      return this.scope.targets;
    }
    get classes() {
      return this.scope.classes;
    }
    get data() {
      return this.scope.data;
    }
    initialize() {}
    connect() {}
    disconnect() {}
    dispatch(eventName, {target: target = this.element, detail: detail = {}, prefix: prefix = this.identifier, bubbles: bubbles = true, cancelable: cancelable = true} = {}) {
      const type = prefix ? `${prefix}:${eventName}` : eventName;
      const event = new CustomEvent(type, {
        detail: detail,
        bubbles: bubbles,
        cancelable: cancelable
      });
      target.dispatchEvent(event);
      return event;
    }
  }
  Controller.blessings = [ ClassPropertiesBlessing, TargetPropertiesBlessing, ValuePropertiesBlessing ];
  Controller.targets = [];
  Controller.values = {};
  class RevealController extends Controller {
    static values={
      open: Boolean,
      transitioning: Boolean,
      targetSelector: String,
      toggleKeys: String,
      showKeys: String,
      hideKeys: String,
      away: Boolean,
      debug: Boolean
    };
    connect() {
      this._initCloseKeypressListener();
      this._initToggleKeypressListener();
      this._initShowKeypressListener();
      this._awayHandler = this._awayHandler.bind(this);
    }
    disconnect() {
      this._teardown();
    }
    show(event) {
      if (this.openValue || this.transitioningValue) return;
      this._init(event, true);
    }
    hide(event) {
      if (!this.openValue || this.transitioningValue) return;
      this._init(event, false);
    }
    toggle(event) {
      if (this.transitioningValue) return;
      this._init(event, !this.openValue);
    }
    async _init(event, shouldOpen) {
      if (event && event.currentTarget && event.currentTarget.dataset) {
        if ("revealPreventDefault" in event.currentTarget.dataset) {
          event.preventDefault();
        }
        if ("revealStopPropagation" in event.currentTarget.dataset) {
          event.stopPropagation();
        }
      }
      const startSelector = `${this.selector}[data-${shouldOpen ? "enter" : "leave"}-start]`;
      const startPromises = this._didInitWithPromise(startSelector, shouldOpen);
      await Promise.all(startPromises);
      const defaultSelector = `${this.selector}:not([data-${shouldOpen ? "enter" : "leave"}-start]):not([data-${shouldOpen ? "enter" : "leave"}-end])`;
      const defaultPromises = this._didInitWithPromise(defaultSelector, shouldOpen);
      await Promise.all(defaultPromises);
      const endSelector = `${this.selector}[data-${shouldOpen ? "enter" : "leave"}-end]`;
      const endPromises = this._didInitWithPromise(endSelector, shouldOpen);
      await Promise.all(endPromises);
    }
    _teardown() {
      if (this.hasAwayValue) {
        document.removeEventListener("click", this._awayHandler);
      }
    }
    _didInitWithPromise(selector, shouldOpen) {
      this._debug("selecting", selector, this.element.querySelectorAll(selector));
      return Array.from(this.element.querySelectorAll(selector)).map((element => this._doInitTransition(element, shouldOpen)));
    }
    _initCloseKeypressListener() {
      if (this.hasHideKeysValue) {
        document.addEventListener("keydown", (event => {
          if (!this.openValue) return;
          if (!this.hideKeysValue.split(",").includes(event.key.toLowerCase())) {
            return;
          }
          event.stopPropagation();
          this.toggle(event);
        }));
      }
    }
    _initToggleKeypressListener() {
      if (this.hasToggleKeysValue) {
        document.addEventListener("keydown", (event => {
          if (!this.toggleKeysValue.split(",").includes(event.key.toLowerCase())) {
            return;
          }
          event.stopPropagation();
          this.toggle(event);
        }));
      }
    }
    _initShowKeypressListener() {
      if (this.hasShowKeysValue) {
        document.addEventListener("keydown", (event => {
          if (this.openValue) return;
          if (!this.showKeysValue.split(",").includes(event.key.toLowerCase())) {
            return;
          }
          event.stopPropagation();
          this.toggle(event);
        }));
      }
    }
    _awayHandler(event) {
      if (!this.element.contains(event.target)) {
        document.removeEventListener("click", this._awayHandler);
        this.hide(event);
      }
      return true;
    }
    _doInitTransition(target, openState) {
      this._debug("init transition", `${openState ? "open" : "closed"}`, target);
      this._debug("dispatching event", `reveal:${openState ? "show" : "hide"}`, target);
      target.dispatchEvent(new Event(`reveal:${openState ? "show" : "hide"}`, {
        bubbles: true,
        cancelable: false
      }));
      return new Promise(((resolve, reject) => {
        if ("transition" in target.dataset && this.element.offsetParent !== null) {
          requestAnimationFrame((() => {
            this._transitionSetup(target, openState);
            const _didEndTransition = this._didEndTransition.bind(this);
            target.addEventListener("transitionend", (function _didEndTransitionHandler() {
              _didEndTransition(target, openState);
              target.removeEventListener("transitionend", _didEndTransitionHandler);
              resolve();
            }));
            requestAnimationFrame((() => {
              this._doStartTransition(target, openState);
            }));
          }));
        } else {
          if (openState) {
            this._debug("force hidden - init", `${openState ? "open" : "closed"}`, target);
            target.hidden = !target.hidden;
          }
          this._doCompleteTransition(target, openState);
          resolve();
        }
      }));
    }
    _doStartTransition(target, openState) {
      this._debug("start transition", `${openState ? "open" : "closed"}`, target);
      this.transitioningValue = true;
      if (target.dataset.useTransitionClasses === "true") {
        const transitionClasses = this._transitionClasses(target, this.transitionType);
        target.classList.add(...transitionClasses.end.split(" "));
        target.classList.remove(...transitionClasses.start.split(" "));
      } else {
        const transitions = this._transitionDefaults(openState);
        target.style.transformOrigin = transitions.origin;
        target.style.transitionProperty = "opacity transform";
        target.style.transitionDuration = `${transitions.duration / 1e3}s`;
        target.style.transitionTimingFunction = "cubic-bezier(0.4, 0.0, 0.2, 1)";
        target.style.opacity = transitions.to.opacity;
        target.style.transform = `scale(${transitions.to.scale / 100})`;
      }
    }
    _didEndTransition(target, openState) {
      this._debug("end transition", `${openState ? "open" : "closed"}`, target);
      if (target.dataset.useTransitionClasses === "true") {
        const transitionClasses = this._transitionClasses(target, this.transitionType);
        target.classList.remove(...transitionClasses.before.split(" "));
      } else {
        target.style.opacity = target.dataset.opacityCache;
        target.style.transform = target.dataset.transformCache;
        target.style.transformOrigin = target.dataset.transformOriginCache;
      }
      this._doCompleteTransition(target, openState);
    }
    _doCompleteTransition(target, openState) {
      this._debug("complete transition", `${openState ? "open" : "closed"}`, target);
      this.transitioningValue = false;
      if (!openState) {
        this._debug("force hidden - complete", `${openState ? "open" : "closed"}`, target);
        target.hidden = !target.hidden;
      }
      this.openValue = openState;
      this._debug("dispatching event", `reveal:${openState ? "shown" : "hidden"}`, target);
      target.dispatchEvent(new Event(`reveal:${openState ? "shown" : "hidden"}`, {
        bubbles: true,
        cancelable: false
      }));
      if (this.hasAwayValue && openState) {
        document.addEventListener("click", this._awayHandler);
      }
      this._debug("dispatching event", "reveal:complete", target);
      target.dispatchEvent(new Event("reveal:complete", {
        bubbles: true,
        cancelable: false
      }));
    }
    _transitionSetup(target, openState) {
      this.transitionType = openState ? "transitionEnter" : "transitionLeave";
      if (this.transitionType in target.dataset) {
        target.dataset.useTransitionClasses = true;
        const transitionClasses = this._transitionClasses(target, this.transitionType);
        target.classList.add(...transitionClasses.before.split(" "));
        target.classList.add(...transitionClasses.start.split(" "));
      } else {
        target.dataset.useTransitionClasses = false;
        const transitions = this._transitionDefaults(openState);
        target.dataset.opacityCache = target.style.opacity;
        target.dataset.transformCache = target.style.transform;
        target.dataset.transformOriginCache = target.style.transformOrigin;
        target.style.opacity = transitions.from.opacity;
        target.style.transform = `scale(${transitions.from.scale / 100})`;
      }
      if (openState) {
        this._debug("opening with transition", target);
        target.hidden = !target.hidden;
      }
    }
    _transitionDefaults(openState) {
      return {
        duration: openState ? 200 : 150,
        origin: "center",
        from: {
          opacity: openState ? 0 : 1,
          scale: openState ? 95 : 100
        },
        to: {
          opacity: openState ? 1 : 0,
          scale: openState ? 100 : 95
        }
      };
    }
    _transitionClasses(target, transitionType) {
      return {
        before: target.dataset[transitionType],
        start: target.dataset[`${transitionType}Start`],
        end: target.dataset[`${transitionType}End`]
      };
    }
    _debug(...args) {
      if (this.debugValue) console.log(...args);
    }
    get selector() {
      return this.hasTargetSelectorValue ? this.targetSelectorValue : "[data-reveal]";
    }
  }
  return RevealController;
}));
